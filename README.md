# Remote and Front-end Analytics

## Build

Run `npm run build`.
* This will create a build in `./dist`. This is an ESM-only target (you can't use this in CommonJS modules).
* This will also make a tarball at `./ucam.uis.devops-ucam-observe-remote-analytics-analytics-x.y.z.tgz`.

## Installation

To install the package, run the following command:

```sh
npm i @ucam.uis.devops/ucam-observe-remote-analytics
```

## Tests

To run the tests, run `npm test`.

## Usage

### CookieBanner

Insert this next to the root of your React app.

> ❗️ Note
>
> Use the `CookieBanner` in a `ThemeProvider` with the `CamMuiTheme` to get the correct style.

__Before__
```jsx
    <html lang="en">
      <body className={inter.className}>
        {children}
      </body>
    </html>
```

__After__
```jsx
    <html lang="en">
      <body className={inter.className}>
        <CookieBanner />
        {children}
      </body>
    </html>
```

### CamMuiTheme

This is a theme that you can use with the `ThemeProvider` from `material-ui`. You should use the provider
in your layout or highest-level React app component.

__Before__
```jsx
    <html lang="en">
      <body className={inter.className}>
        <CookieBanner />
        {children}
      </body>
    </html>
```

__After__
```jsx
    <html lang="en">
      <body className={inter.className}>
        <ThemeProvider theme={CamMuiTheme}>
          <CssBaseline />
          <CookieBanner />
          {children}
        </ThemeProvider>
      </body>
    </html>
```

## Google Analytics Access

To gain access to Google Analytics and obtain your GA tracking code, follow these steps:

1. **Login to Google Workspace:**
   Use your `@cam.ac.uk` Cambridge account to log in via the [Google Workspace Service Preferences](https://preferences.g.apps.cam.ac.uk/).

2. **Enable Google Analytics:**
   In the Service Preferences web app, navigate to the "Google Analytics" section and enable it to activate your 
access to Google Analytics 4 (GA4).

3. **Obtain Your GA Tracking Code:**
   - Once enabled, go to the [Google Analytics](https://analytics.google.com/) dashboard.
   - Create a new GA4 property if you haven't already.
   - After setting up the property, you will receive a tracking ID in the format `G-XXXXXXX`. This is the code you will 
use to integrate Google Analytics into your project.

4. **Manage Team Access:**
   - Ensure that the correct GA4 property is selected before proceeding.
   - Navigate to the Admin section in your Google Analytics by selecting the gear icon at the bottom left.
   - Go to 'Account Settings' -> 'Account Access Management' to add, edit, or remove team members.

5. Use the obtained GA tracking code (e.g., `G-MJKN0LGYW0`) in your project by configuring it within the analytics 
setup of your application.

If you encounter any issues or require further assistance, please refer to the 
[Setting up Google Analytics and Google Search Console](https://help.uis.cam.ac.uk/service/websites-and-intranets/websites/setting-google-analytics-and-google-search-console) 
documentation.

## Examples

In the `basic-usage` folder, you will find an example application.

To use it, start from the root folder, then run the following to install the package:

```zsh
npm run build
pushd && cd basic-usage
npm i ../ucam.uis.devops-ucam-observe-remote-analytics-0.1.4.tgz
npm i
```

Then run the following to start the test app:

```zsh
npm run dev
```

Go to `localhost:3000` to see the application.

## Contributing

This project is [MIT](LICENSE) licensed.