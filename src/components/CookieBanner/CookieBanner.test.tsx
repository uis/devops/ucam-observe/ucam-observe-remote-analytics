/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render } from '@testing-library/react';
import CookieBanner from './CookieBanner';

describe('Snapshots', () => {
  it('renders CookieBanner correctly', () => {
    const { container } = render(<CookieBanner />);
    expect(container).toMatchSnapshot();
  });
});
