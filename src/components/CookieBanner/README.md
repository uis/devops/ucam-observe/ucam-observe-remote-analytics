# Compliance

## `uoc_cookie_choices-version`

This cookie must be set to the latest compliant version by the CookieBanner component.

That version is currently 0.1.1.

## `uoc_cookie_choices-categories`

This must be a string representation of a JSON array of strings. Each string can be either:
* "personalisation_settings"
* "social_marketing_preferences"

The strings cannot be duplicated.

## `uoc_cookie_choices`

This must be the length of the JSON array representation of `uoc_cookie_choices-categories`.