declare global {
  interface Window {
    dataLayer: unknown[];
  }
}

export function initGoogleAnalytics(trackingId: string) {
  if (!trackingId) {
    console.warn('GA Tracking ID is missing');
    return;
  }
  if (!document.querySelector(`script[src*='${trackingId}']`)) {
    const script = document.createElement('script');
    script.async = true;
    script.src = `https://www.googletagmanager.com/gtag/js?id=${trackingId}`;
    document.head.appendChild(script);

    script.onload = () => {
      window.dataLayer = window.dataLayer || [];
      function gtag(...args: unknown[]) {
        window.dataLayer.push(args);
      }
      gtag('js', new Date());
      gtag('config', trackingId);
    };
  }
}
