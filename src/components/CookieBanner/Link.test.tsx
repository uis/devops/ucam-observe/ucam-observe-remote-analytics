/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render } from '@testing-library/react';

import Link from './Link';

describe('Snapshots', () => {
  it('renders ButtonWrapper correctly', () => {
    const element = render(
      <Link dark href="https://www.cam.ac.uk/about-this-site/privacy-policy">
        Give me more information
      </Link>
    );
    expect(element).toMatchSnapshot();
  });
});
