Feature: CookieBanner
  As a developer
  I want to be able to add a style-conformant banner to a React app
  And that banner needs to prompt end users about their cookie preferences

  Background:
    Given the CookieBanner component has been added to a page

  Scenario: Prompted if no cookie preferences set
    Given no cookie preferences are set
    When I go to the page
    Then I see the CookieBanner

  Scenario: Cookie preferences are stored
    Given I see the CookieBanner
    When I set my preferences
    Then those preferences are stored

  Scenario: Not prompted if cookie preferences set
    Given cookie preferences are set
    When I go to the page
    Then I do not see the CookieBanner

  Scenario: Clearing cookies clears cookie preferences
    Given I clear all cookies
    When I go to the page
    Then I do not see the CookieBanner

  Scenario: Compliance
    Given I see the CookieBanner
    When I set my preferences
    Then the cookies are set according to the compliance info in CookieBanner/README.md
