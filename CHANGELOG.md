# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# [0.1.4] - 2024-12-07

## Changed
- Modified `src/index.ts` to export `utils`, `components`, `consts` and `themes` namespaces.
- Added `types` exports to `package.json` to ensure that TypeScript types are included in the published package.

# [0.1.3] - 2024-11-15

## Changed
- Modified `.gitlab-ci.yml`:
  - Added caching for the `dist/` directory.
  - Rename `install_dependencies` job to `build`.
  - Added a new `.publish_script` to standardize publishing preparation steps across jobs.
  - Added a new `publish_test` job to check for version changes and verify the existence of the `dist` directory before performing a dry run of the publication process.
- Updated `jest.config.cjs` to include `testPathIgnorePatterns` for `/dist/` and `/node_modules/` directories, ensuring these are not processed during testing.

# [0.1.2] - 2024-11-15

## Added
- Included `files` field in `package.json` to explicitly specify the files to include in the npm package. 
This ensures that only the `dist/**/*` files are packaged during publication.

# [0.1.1] - 2024-11-14

## Added
- Google Analytics setup function to provide a standardized mechanism for initializing Google Analytics across web applications.

## Changed
- Updated `package.json` to define main module paths and export conditions, supporting both ESM and CJS modules.
- Improved import paths in example apps to use the new namespace.
- Updated `README.md` to reflect the new version of the package in installation instructions.


# [0.1.0] - 2024-07-15

## Added

- CI
  - Automatic publishing to npm via GitLab CI.


# [0.1.0] -- UNRELEASED

## Added

- Components
  - `CookieBanner` component for approved banner for cookie preferences.
- Themes
  - `CamMuiTheme` material-ui theme for University of Cambridge styles.
- Examples
  - `basic-usage` test app showing some of the functionality used in this library.