/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render } from '@testing-library/react';

import Checkbox from './Checkbox';

describe('Snapshots', () => {
  it('renders Checkbox correctly', () => {
    const element = render(
      <Checkbox
        dark
        label="Personalisation settings"
        checked
        onChange={() => {}}
        inputProps={{
          'aria-describedby': 'personalisation-description'
        }}
        data-test-id="personalisation-checkbox"
      />
    );
    expect(element).toMatchSnapshot();
  });
});
