export const UOC_COOKIE_CHOICES = 'uoc_cookie_choices';
export const UOC_COOKIE_CHOICES_VERSION = 'uoc_cookie_choices-version';
export const UOC_COOKIE_CHOICES_CATEGORIES = 'uoc_cookie_choices-categories';
export const PERSONALISATION_SETTINGS = 'personalisation_settings';
export const SOCIAL_MARKETING_PREFERENCES = 'social_marketing_preferences';
