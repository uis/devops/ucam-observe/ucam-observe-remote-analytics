/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render } from '@testing-library/react';

import ButtonWrapper from './ButtonWrapper';
import Button from './design-system/Button';

describe('Snapshots', () => {
  it('renders ButtonWrapper correctly', () => {
    const element = render(
      <ButtonWrapper sx={{ mb: { xs: 0, sm: 0 } }}>
        <Button id="dismiss-banner-button" color="secondary" onClick={() => {}}>
          Save my choices
        </Button>
      </ButtonWrapper>
    );
    expect(element).toMatchSnapshot();
  });
});
