/**
 * Thin shim to replicate unused functionality from gatsby
 */
import React from 'react';

// eslint-disable-next-line
export type GatsbyLinkProps<T> = any;

// eslint-disable-next-line
export function Link(...props: any[]) {
  return <></>;
}
