/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render } from '@testing-library/react';

import Button from './Button';

describe('Snapshots', () => {
  it('renders Button correctly', () => {
    const element = render(
      <Button id="dismiss-banner-button" color="secondary" onClick={() => {}}>
        Save my choices
      </Button>
    );
    expect(element).toMatchSnapshot();
  });
});
